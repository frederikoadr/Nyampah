﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeteksiSampah : MonoBehaviour
{
    public string NameTag;
    public AudioClip AudioBenar, AudioSalah;
    private AudioSource mediaPlayerBenar, mediaPlayerSalah;
    public Text TextScore;

    // Start is called before the first frame update
    private void Start()
    {
        mediaPlayerBenar = gameObject.AddComponent<AudioSource>();
        mediaPlayerBenar.clip = AudioBenar;
        mediaPlayerSalah = gameObject.AddComponent<AudioSource>();
        mediaPlayerSalah.clip = AudioSalah;
    }

    //Mendeteksi trigger collision untuk mendeteksi sampah
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals(NameTag))
        {
            Data.Score += 25;
            TextScore.text = Data.Score.ToString();
            Destroy(collision.gameObject);
            mediaPlayerBenar.Play();
        }
        else
        {
            Data.Score -= 5;
            TextScore.text = Data.Score.ToString();
            Destroy(collision.gameObject);
            mediaPlayerSalah.Play();
        }
    }
}
