﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MunculkanSampah : MonoBehaviour
{
    public float Jeda = 0.8f;
    float timer;
    public GameObject[] ObyekSampah;

    // Update is called once per frame
    private void Update()
    {
        timer += Time.deltaTime;
        if (timer > Jeda)
        {
            int random = Random.Range(0, ObyekSampah.Length);
            Instantiate(ObyekSampah[random], transform.position, transform.rotation);
            timer = 0;
        }
    }
}
