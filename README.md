# Nyampah
Nyampah is a simple game implementation on Android that is played by sorting between organic and non-organic waste.🚮

Engine : [![Made with Unity](https://img.shields.io/badge/unity-%23000000.svg?style=for-the-badge&logo=unity&logoColor=white)](https://unity3d.com) 2019.4.11f1 (LTS)

Language : [![](https://img.shields.io/badge/c%23-%23239120.svg?style=for-the-badge&logo=c-sharp&logoColor=white)](https://docs.microsoft.com/en-us/dotnet/csharp/)


Platform : [![Android](https://img.shields.io/badge/Android-3DDC84?style=for-the-badge&logo=android&logoColor=white)](https://developer.android.com/docs)

## Gameplay
![](Gameplay.gif)